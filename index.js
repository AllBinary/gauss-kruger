/* Based on: */
/* Javascript for Gauss-Kruger projection, Bo Justusson, SCB, 2007-03-13 (started 2002-03-29) */
/* Adapted as a nodejs module by: All Binary AB 2014 */

exports = module.exports = function() {

    var LL = { Lat:60 , Long:15 }; /* used to return value in function RT90ToLatLong */
    var XY = { x:6600000, y:1500000 }; /* dito in LatLongToRT90 */
    var x0,y0,k0,a,f1,lamda0; /* Ellipsoid params */
    var f,n,e2,an,ak, d1,d2,A1,B1, A2,B2,b1,b2; /*,b3;*/ /*basic dervied coefficients */

    function CompProjCoeffs() {/* compute some basic coefficients */
	f  = 1/f1;
	n  = f/(2-f);
	e2 = f*(2-f);
	/* first and second order terms in series, errors < 1 meter: */
	an = a/(1+n)*( 1+n*n/4 ); /*+n*n*n*n/64); */
	ak = k0*an;
	/* coeff for RT90-x,y to lat,long */
	d1 = n/2 - n*n*2/3  ; 
	d2 = n*n/48  ;
	A1 = e2 + e2*e2  ; 
	B1 = -1/6*( 7*e2*e2 );
	/* coeff for lat,long to RT90-x,y */
	A2 = e2 ;
	B2 = 1/6*(5*e2*e2);   /* - e2*e2*e2);*/
	b1 = n/2 - n*n*2/3;   /* +5/16*n*n*n;*/
	b2 = 13/48*n*n;       /* -3/5*n*n*n; */
	/* b3 = 61/240*n*n*n; */
    }

    function atanh(y){ 
	var x = 0.5*Math.log( (1+y)/(1-y) );
	return x;
    }

    return {
	SetSWEREF99WGS84: function() {/* Params for SWEREF99 WGS84 (Lantm�teriet standard TM, ellipsoid GRS80) */
	    x0 = 0;                 /* false northing */
	    y0 = 500000;            /* false easting */
	    k0 = 0.9996;            /* enlargment factor */
	    a  = 6378137.0          /* half major ellipsoid axis, Ellipsoid GRS80*/
	    f1 = 298.257222101;     /* 1/f, f=(a-b)/a */
	    lamda0 = 15.0 * Math.PI /180 ; /* radians, 15 degrees E */
	    CompProjCoeffs();
	},
	
	SetRT90Bessel: function() {/* Parameters for RT90 Bessel*/
	    x0 = 0;                /* false northing */
	    y0 = 1500000;          /* false easting */
	    k0 = 1.0;              /* enlargment factor */
	    a  = 6377397.155       /* half major ellipsoid axis */
	    f1 = 299.1528128;      /* 1/f, f=(a-b)/a */
	    lamda0 = 17.564753086 * Math.PI /200 ; /* (obs nygrader /200), radians 2.5 gon V Stockholm observatorium*/
            /* i vanliga grader, 15�48'29".8 �st Greenwich*/
	    CompProjCoeffs();
	},

	SetRT90WGS84: function() { /* Params for RT90 WGS84 (Lantm�teriet approx. SWEREF 99) */
	    x0 = -667.711;         /* false northing */
	    y0 = 1500064.274;      /* false easting */
	    k0 = 1.00000561024;    /* enlargment factor */
	    a  = 6378137.0         /* half major ellipsoid axis */
	    f1 = 298.257222101;    /* 1/f, f=(a-b)/a */
	    lamda0 = 15.806284529 * Math.PI /180 ; /* radians, close to 2.5 gon V Stockholm observatorium */
	    CompProjCoeffs();
	},
	XYtoLatLong: function(X,Y) { /* output fi=latitude, dlambda=lat-lat0 */
	    xi = (X-x0)/ak;   yi = (Y-y0)/ak;

	    /* comp hyperbolic functions */
	    var e2y = Math.exp(2*yi);       var e4y = e2y*e2y;  
	    var cosh2y = (e2y + 1/e2y)/2;   var cosh4y = (e4y + 1/e4y)/2;
	    var sinh2y = (e2y - 1/e2y)/2;   var sinh4y = (e4y - 1/e4y)/2;

	    /* comp adjusted xi,yi */
	    xi1 = xi - d1*Math.sin(2*xi)*cosh2y - d2*Math.sin(4*xi)*cosh4y; 
	    yi1 = yi - d1*Math.cos(2*xi)*sinh2y - d2*Math.cos(4*xi)*sinh4y;

	    /* comp hyperbolics for yi */
	    var eyi=Math.exp(yi1); var coshyi=(eyi+1/eyi)/2; var sinhyi=(eyi-1/eyi)/2;
	    fi0 = Math.asin( Math.sin(xi1)/coshyi );
	    dlamda = Math.atan( sinhyi /Math.cos(xi1) );

	    /* transform isometric latitude to lat */ 
	    var sinfi0=Math.sin(fi0);   var sin2fi0=sinfi0*sinfi0;
	    fi = fi0 + sinfi0*Math.cos(fi0)*( A1 + B1*sin2fi0 );
	    lamda = dlamda + lamda0;

	    return {Lat: fi * 180 / Math.PI,
		    Long: lamda * 180 / Math.PI};
	},
	LatLongtoXY: function(Lat,Long) { 
	    /* Input Lat,Long in decimal degrees, Output {X: x, Y: y} */
	    fi = Lat * Math.PI / 180;
	    lamda = Long * Math.PI / 180;
	    dlamda = lamda - lamda0;
	    /* transform latitude to isometric lat */ 
	    var sinfi=Math.sin(fi);   var sin2fi=sinfi*sinfi;
	    fi0 = fi - sinfi*Math.cos(fi)*( A2 + B2*sin2fi );
	    xi = Math.atan( Math.tan(fi0)/Math.cos(dlamda) );
	    yi = atanh( Math.cos(fi0)*Math.sin(dlamda) );
	    /* comp hyperbolic functions */
	    var e2y = Math.exp(2*yi);       var e4y = e2y*e2y;  
	    var cosh2y = (e2y + 1/e2y)/2;   var cosh4y = (e4y + 1/e4y)/2;
	    var sinh2y = (e2y - 1/e2y)/2;   var sinh4y = (e4y - 1/e4y)/2;
	    /* var sinh6y = (e4y*e2y - 1/(e4y*e2y) )/2;*/
	    /* comp adjusted xi,yi */
	    xi1 = xi + b1*Math.sin(2*xi)*cosh2y + b2*Math.sin(4*xi)*cosh4y; 
	    yi1 = yi + b1*Math.cos(2*xi)*sinh2y + b2*Math.cos(4*xi)*sinh4y;
            /*+ b3*Math.cos(6*xi)*sinh6y;*/

	    /* Comp output x,y , FIX -0.3 = 3 dm */
	    return {X: (xi1*ak + x0 -0.3),
		    Y: yi1*ak + y0 };
	},
	DegToDM: function(deg) {
	    deg = Math.round(deg*500000)/500000;
	    dr = Math.floor(deg);
	    m = (deg-dr)*100*0.6; 
	    m = Math.round(m*1000000)/1000000;
	    mstr=m.toString(); 
	    if (mstr[0]=='.') mstr = '0'+mstr;
	    dm = ''+ dr.toString() +': '+ mstr;
	    /* TEST alert('deg='+deg +' dr='+dr +' mr='+mr +' sr='+sr +'\n'
               +'DD:MM:SS=' + dms  ); */
	    return dm;
	},
	DMToDeg: function(DM) { /* DM=DD:M.mmm */
	    DMarray = DM.split(':');
	    if (DMarray[0].length > 2) DMarray = DM.split(' ');
	    D = eval(DMarray[0]);
	    M = eval(DMarray[1]);
	    deg = D + M/60;
	    return deg;
	},
	DegToDMS: function(deg) {
	    deg = Math.round(deg*500000)/500000;
	    dr = Math.floor(deg);
	    m = (deg-dr)*100*0.6; 
	    mr = Math.floor(m);
	    s = (m-mr) *100*0.6;
	    sr = Math.round(s*100)/100; 
	    dms = ''+ dr.toString() +':'+ mr.toString()+':'+sr.toString();
	    return dms;
	},
	DMSToDeg: function(D,M,S) {
	    deg = D + M/60 + S/3600;
	    return deg;
	}

    };
};
